在这个应用程序中，创建了以下文件。
* index.html - Web应用程序首页。
* link.html - 链接页面。
* login.html - 登录页面。
* LoginServlet.java - 登录Servlet处理。
* LogoutServlet.java - 注销Servlet处理。
* ProfileServlet.java - 用户个人资料Servlet。
* web.xml - Servlet配置文件。


